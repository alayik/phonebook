<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PhonebookRequest;
use App\Phone;
use App\User;

class PhonebookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'success' => true,
            'message' => 'all phonebook successfully retrieved',
            'data' => Phone::select(['id', 'number', 'type'])->get()->toJson(),
        ]);
    }

    /**
     * Display a listing of user's phonebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function userPhonebook(User $user)
    {
        return response()->json([
            'success' => true,
            'message' => "User's phonebook successfully retrieved",
            'data' => $user->phoneNumbers()
                ->select(['id', 'number', 'type'])
                ->get()
                ->makeVisible('number')
                ->toJson(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\PhonebookRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhonebookRequest $request)
    {
        if (User::where('email', $request->input('user_id'))->exists()) {
            return response()->json([
                'success' => false,
                'message' => 'The user ID is incorrect',
            ], 404);
        }

        $phone = Phone::create($request->only(['number', 'type', 'user_id']));

        if ($phone) {
            return response()->json([
                'success' => true,
                'message' => 'Phone Number successfully created',
                'data' => $phone->makeVisible('number')->only(['id', 'number', 'type']),
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Phone Number could not be created',
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function show(Phone $phone)
    {
        return response()->json([
            'success' => true,
            'message' => 'Phone Number successfully retrieved',
            'data' => array_merge($phone->makeVisible('number')->only(['id', 'number', 'type']), ['user' => $phone->user->name]),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\PhonebookRequest  $request
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function update(PhonebookRequest $request, Phone $phone)
    {
        if ($phone->user->id != $request->input('user_id')) {
            return response()->json([
                'success' => false,
                'message' => 'The user ID is incorrect',
            ], 404);
        }

        $updated = $phone->fill($request->only(['number', 'type']))->save();

        if ($updated) {
            return response()->json([
                'success' => true,
                'message' => 'Phone successfully updated',
                'data' => $phone->refresh()->makeVisible('number')->only(['id', 'number', 'type']),
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Phone could not be updated',
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Phone $phone)
    {
        if ($phone->delete()) {
            return response()->json([
                'success' => true,
                'message' => "Phone successfully deleted",
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => "Phone could not be deleted",
            ], 500);
        }
    }
}
