<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequset;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return response()->json([
            'success' => true,
            'message' => 'Users successfully retrieved',
            'data' => User::select(['id', 'name', 'email'])->get()->toJson(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequset $request)
    {
        $user = User::create(
            $request->merge(['password' => bcrypt($request->input('password'))])
                ->only(['name', 'email', 'password'])
        );

        if ($user) {
            return response()->json([
                'success' => true,
                'message' => 'User successfully created',
                'data' => $user->only(['id', 'name', 'email']),
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'User could not be created',
            ], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return response()->json([
            'success' => true,
            'message' => 'User successfully retrieved',
            'data' => $user->only(['id', 'name', 'email']),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UserRequset  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequset $request, User $user)
    {

        $updated = $user->fill(
            $request->merge(['password' => bcrypt($request->input('password'))])
                ->only(['name', 'email', $request->input('password') ? '' : 'password'])
        )->save();

        if ($updated) {
            return response()->json([
                'success' => true,
                'message' => 'User successfully updated',
                'data' => $user->refresh()->only(['id', 'name', 'email']),
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'User could not be updated',
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'User successfully deleted',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'User could not be deleted',
            ], 500);
        }
    }
}
