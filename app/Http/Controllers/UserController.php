<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index');
    }
    /**
     * Display a listing of the user's phonebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function phonebook(User $user)
    {
        return view('user.phonebook', compact('user'));
    }
}
