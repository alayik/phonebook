<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequset extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => [
                'required', 'email',
                /**
                 * The current user's email is ignored in the list of emails being reviewed
                 */
                Rule::unique((new User)->getTable())->ignore($this->route()->user->id ?? null),
            ],
            'password' => [
                /**
                 * When we update the user we do not need to have the email required
                 * and being empty means not changing the password.
                 */
                $this->route()->user ? 'nullable' : 'required', 'min:6',
            ],
        ];
    }
}
