module.exports = {
    prefix: 'tw-', //tailwind css
    theme: {
        colors: {
            white: '#ffffff',
            black: '#000000',
            dark: '#1d1c1b',
            smoke: '#ebebeb',
            grey: '#808080',
            transparent: 'transparent',
            green: {
                'darkest': '#097431',
                'darker': '#0c9940',
                'default': '#0fbd4f',
                'lighter': '#12e15e',
                'lightest': '#2cee74',
            },
            blue: {
                'darkest': '#1C3D5A',
                'darker': '#2779BD',
                'default': '#3490DC',
                'lighter': '#6CB2EB',
                'lightest': '#BCDEFA',
            },
            purple: {
                'darkest': '#602e75',
                'darker': '#773991',
                'default': '#8E44AD',
                'lighter': '#a15abe',
                'lightest': '#b176c9',
            },
            yellow: {
                'darkest': '#baab05',
                'darker': '#e1ce06',
                'default': '#f9e614',
                'lighter': '#faea3a',
                'lightest': '#fbee61',
            },
            red: {
                'darkest': '#a30d0d',
                'darker': '#c81010',
                'default': '#ec1313',
                'lighter': '#ef3737',
                'lightest': '#f25c5c',
            },

        },
        extend: {}
    },
    variants: {},
    plugins: [],
    corePlugins: {
        placeholderColor: false,
        accessibility: false,
        fontSmoothing: false,
        backgroundRepeat: false,
        backgroundPosition: false,
        backgroundSize: false,
        backgroundAttachment: false,
    }
}
