@extends('layouts.master')

@section('title',"Phonebook | {$user->name}")

@section('content')
 	<section class="head tw-mb-8">
		<h2 class="tw-text-2xl tw-text-center tw-m-2 tw-text-dark tw-font-bold tw-uppercase">
	        <span class="tw-border-b tw-border-dashed tw-border-grey">'{{ $user->name }}' Phonebook</span>
	    </h2>
 	</section>
 	<div class="tw-max-w-xl tw-mx-auto ">
	 	<phonebook-form class="tw-mb-6" :user-id="{{ $user->id }}"></phonebook-form>
	 	<hr class="tw-border-dashed tw-border-grey tw-my-6">
		<phonebook-list :user-id="{{ $user->id }}"></phonebook-list>
 	</div>
@endsection
