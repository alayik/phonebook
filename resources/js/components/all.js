/*
|--------------------------------------------------------------------------
| Application Components
|--------------------------------------------------------------------------
|
|	Here is where you can register all of the components for an application.
|
*/

import { UserList, UserForm } from "./users"
import { PhonebookList, PhonebookForm } from "./phonebook"

let list = [
    UserList, UserForm,
    PhonebookList, PhonebookForm
];

list.map(component => {
    Vue.component(component.name, component)
});
