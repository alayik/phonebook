<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Phone;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

$factory->define(Phone::class, function (Faker $faker) {
    return [
        'number' => $faker->phoneNumber,
        'type' => Arr::random(['home', 'company', 'work']),
    ];
});

$factory->state(Phone::class, 'user', function ($faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
    ];
});
