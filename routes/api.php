<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::namespace ('Api')->name('api.')->group(function () {
    Route::apiResource('user', 'UserController');

    Route::get('user/{user}/phonebook', 'PhonebookController@userPhonebook')->name('user.phonebook');

    Route::apiResource('phonebook', 'PhonebookController', ['parameters' => [
        'phonebook' => 'phone',
    ]]);
});
